# docker-marketplace
### Клонирование проекта с подмодулями
```
git clone --recursive git@gitlab.com:naruto42/docker-marketplace.git
```

### Запуск и сборка контенейров
В системе должен быть установлен docker и docker-compose

Для успешной сборки контейнеров должны быть не заняты следующие
порты: 3306, 80, 443.

Берем файл:
```
<директория проекта>/data/mysql.zip
```
Распаковываем в папку, если ее нет, то создаем и распаковываем
```
<директория проекта>/docker/
```

Для того, чтобы поднять проект, выполните слеудющую команду:
```
docker-compose up -d --build
```

### Настройка хостов
В файле hosts добавить запись.
```
127.0.0.1 market.local              # Клиент
127.0.0.1 admin.market.local        # Админка
127.0.0.1 api.market.local          # API
127.0.0.1 client.doc.market.local   # Документация клиент
127.0.0.1 admin.doc.market.local    # Документация админки
127.0.0.1 client.logic.market.local # Бизнес логика (Клиент)
127.0.0.1 admin.logic.market.local  # Бизнес логика (Админка)
```

### Настройка API
Переходим в директорию:
```
<директория проекта>/src/back/api-marketplace/
```
Копируем .env.example в .env

Далее в этом файле нужно настроить следующие разделы 
(параетры для оплаты через сбербанк можно оставить без изменений,
если они валидны):

####Почта
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null
```

####Настройки подключения для оплаты через сбербанк
```
SBERBANK_NAME=kenguruexpress-operator
SBERBANK_PASSWORD=kenguruexpress
SBERBANK_TOKEN=tqvh67ain0alo2dvks5ptc6guj
SBERBANK_API=https://3dsec.sberbank.ru
```

####Настройки обратных ссылок после оплаты
```
ACQUIRING_URL_SUCCESS=https://market.local/orders
ACQUIRING_URL_FAILS=https://market.local/orders
```

####Установка пакетов
Заходим в контейнер php
```
docker exec -it market_php_fpm /bin/bash
```
Переходим в директорию:
```
cd /var/www/src/back/api-marketplace
```
Запускаем команду:
```
composer install
```
Конфигурирование laravel passport
```
artisan passport:install
```
Создание ссылки на ресурсы
```
php artisan storage:link
```
Если нужно запустить проект без данных:
```
php artisan migrate
```
Если нужны данные, то берем файл
```
<директория проекта>/data/img.zip
``` 
Распаковываем в директорию:
```
<директория проекта>/src/api-marketplace/storage/public/
```

### Тесты
Войти в контейнер:
```
docker exec -it market_php_fpm /bin/bash
```
Перейти в каталог
```
cd /var/www/src/back/api-marketplace/
```
Запустить тесты
```
php vendor/bin/phpunit
```

### Открыти проекта
Переходим по ссылкам в google chrome:

https://api.market.local

На каждой странице будет отображена ошибка подключения, так как используется
самоподписной сертификат.
На странице нажимаем по кнопке "Дополнительные" и ниже
нжимаем "Перейти на сайт api.market.local"

Такие шаги проделываем с другими страницами:

https://market.local

https://admin.market.local

### URL адреса проекта доступные после запуска контейнеров
https://market.local - Клиент

https://admin.market.local - Админка

https://api.market.local - API

http://client.doc.market.local - Документация клиент

http://admin.doc.market.local  - Документация админки

http://client.logic.market.local - Бизнес логика (Клиент)

http://admin.logic.market.local - Бизнес логика (Админка)